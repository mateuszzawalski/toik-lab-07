import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;


import java.io.FileNotFoundException;
import java.io.FileOutputStream;


public class Main {
    public static void main(String[] args) throws FileNotFoundException, DocumentException {
        System.out.println("Hello World!"); // Display the string.

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);

        // 2. Create PdfWriter
        PdfWriter.getInstance(document, new FileOutputStream("result.pdf"));

        // 3. Open document
        document.open();

        // 4. Add content
        Paragraph title = new Paragraph("\nResume\n\n", new Font(Font.getFamily("Arial"), 20.0f));
        title.setAlignment(Element.ALIGN_CENTER);


        document.add(title);


        PdfPTable table = new PdfPTable(2);
        PdfPCell cells [] = {new PdfPCell(new Phrase("First Name")),
                new PdfPCell(new Phrase("Mateusz")),
                new PdfPCell(new Phrase("Last name")),
                new PdfPCell(new Phrase("Zawalski")),
                new PdfPCell(new Phrase("Profession")),
                new PdfPCell(new Phrase("Java Developer")),
                new PdfPCell(new Phrase("Education")),
                new PdfPCell(new Phrase("2018 - 2022 - PWSZ")),
                new PdfPCell(new Phrase("Summary")),
                new PdfPCell(new Phrase("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus condimentum leo et lorem sodales sagittis ut a turpis. Donec suscipit, lectus ut imperdiet ultrices, ante lorem sodales lacus, eget tincidunt lorem erat sed nisi. Nulla vulputate metus sed tincidunt maximus. Aliquam et dui nec neque consectetur consequat id non metus."))};

        for(PdfPCell c : cells){
            c.setPadding(10);
            table.addCell(c);
        }

        document.add(table);
        // 5. Close document
        document.close();
    }
}
